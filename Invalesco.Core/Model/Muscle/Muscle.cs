﻿using Invalesco.Core.Model.Common;

namespace Invalesco.Core.Model.Muscle
{
    /// <summary>
    /// Muscle - Muscle is a soft tissue found in most animals. Muscle cells contain protein filaments of actin 
    /// and myosin that slide past one another, producing a contraction that changes both the length and the shape of the cell. 
    /// </summary>
    public class Muscle : InvalescoEntity
    {
        public virtual MuscleGroup MuscleGroup { get; set; }
        public virtual MuscleName MuscleName { get; set; }

        public Muscle()
            : base()
        { }
    }
}
