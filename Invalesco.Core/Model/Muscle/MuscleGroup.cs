﻿using Invalesco.Core.Model.Common;
using System.Collections.Generic;

namespace Invalesco.Core.Model.Muscle
{
    /// <summary>
    /// Muscle Group - a collection of muscles related by a particular function and location on the body
    /// </summary>
    public class MuscleGroup : InvalescoEntity
    {
        public virtual IList<Muscle> Muscles { get; set; }
        public virtual MuscleGroupName MuscleGroupName { get; set; }

        public MuscleGroup()
            : base()
        {
            Muscles = new List<Muscle>();
        }
    }
}
