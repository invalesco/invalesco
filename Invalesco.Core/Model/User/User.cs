﻿using Invalesco.Core.Model.Common;
using System;
using System.ComponentModel.DataAnnotations;

namespace Invalesco.Core.Model.User
{
    public class User : InvalescoEntity
    {
        [DataType(DataType.EmailAddress)]
        public virtual String EmailAddress { get; set; }
        public virtual String Password { get; set; }
        public virtual String Username { get; set; }
    }
}
