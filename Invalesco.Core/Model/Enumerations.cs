﻿
namespace Invalesco.Core.Model
{
    /// <summary>
    /// Muscle Involvement - defines the involvement of a muscle in an exercise
    /// </summary>
    public enum MuscleInvolvement
    {
        Invalid = 0,
        Target = 1,
        Synergist = 2,
        DynamicStabilizer = 3,
        Stabilizer = 4,
        AntagonistStabilizer = 5
    }

    /// <summary>
    /// Muscle Name - Name of muscle, useful for programmatic identification of particular muscles
    /// </summary>
    public enum MuscleName
    {
        Invalid = 0,
        RectusFemoris = 1,
        VastusIntermedius = 2,
        VastusLaterialis = 3,
        VastusMedialis = 4,
        GluteusMaximus = 5
    }

    /// <summary>
    /// Muscle Name - Name of muscle, useful for programmatic identification of particular muscles
    /// </summary>
    public enum MuscleGroupName
    {
        Invalid = 0,
        Quadriceps = 1,
        Gluteals = 2
    }
}
