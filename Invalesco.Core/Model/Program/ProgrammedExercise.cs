﻿using Invalesco.Core.Model.Common;

using E = Invalesco.Core.Model.Exercise;

namespace Invalesco.Core.Model.Program
{
    public class ProgrammedExercise : InvalescoEntity
    {
        public virtual E.Exercise Exercise { get; set; }
        public virtual Program Program { get; set; }
    }
}
