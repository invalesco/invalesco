﻿using Invalesco.Core.Model.Common;
using System.Collections.Generic;

namespace Invalesco.Core.Model.Program
{
    public class Program : InvalescoEntity
    {
        public virtual IList<ProgrammedExercise> ProgrammedExercises { get; set; }

        public Program()
        {
            ProgrammedExercises = new List<ProgrammedExercise>();
        }
    }
}
