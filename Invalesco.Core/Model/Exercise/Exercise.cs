﻿using Invalesco.Core.Model.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Invalesco.Core.Model.Exercise
{
    /// <summary>
    /// Exercise - any bodily activity that enhances or maintains physical fitness and overall health and wellness
    /// </summary>
    
    [Table("Exercises")]
    public class Exercise : InvalescoEntity
    {
        public virtual IList<ExerciseMuscle> ExerciseMuscles { get; set; }

        public Exercise()
        {
            ExerciseMuscles = new List<ExerciseMuscle>();
        }
    }
}
