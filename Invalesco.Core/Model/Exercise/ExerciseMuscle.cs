﻿using Invalesco.Core.Model.Common;

using M = Invalesco.Core.Model.Muscle;

namespace Invalesco.Core.Model.Exercise
{
    /// <summary>
    /// Exercise Muscle Group - helps us define the many to many relationship between exercises and muscle groups
    /// </summary>
    public class ExerciseMuscle : InvalescoEntity
    {
        public virtual Exercise Exercise { get; set; }
        public virtual M.Muscle Muscle { get; set; }
        public virtual MuscleInvolvement MuscleInvolvement { get; set; }
    }
}
