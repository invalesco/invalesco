﻿using System;
using System.Text;

namespace Invalesco.Core.Model.Common
{
    /// <summary>
    /// Name - usually the name of an entity
    /// </summary>
    public class Name : InvalescoEntity
    {
        public virtual String FirstName { get; set; }
        public virtual String SecondName { get; set; }
        public virtual String LastName { get; set; }

        public Name(String firstName)
            : base()
        {
            FirstName = firstName;
        }

        public Name()
            : base()
        { }

        public Name(string firstName, string secondName, string lastName)
        {
            FirstName = firstName;
            SecondName = secondName;
            LastName = lastName;
        }

        public override String ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.Append(FirstName)
                         .Append(SecondName)
                         .Append(LastName);

            return stringBuilder.ToString();
        }
    }
}
