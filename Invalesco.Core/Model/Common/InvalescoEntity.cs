﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Invalesco.Core.Model.Common
{
    /// <summary>
    /// InvalescoEntity - basic data required by all entities of invalesco
    /// </summary>
    public class InvalescoEntity
    {
        [Key]
        public long Id { get; set; }
        public virtual long ExternalIdentifier { get; set; }
        public virtual DateTime? UpdatedDate { get; set; }
        public virtual Name EntityName { get; set; }
        public virtual string Description { get; set; }
        public virtual string Comments { get; set; }
        public virtual IList<InvalescoImage> Images { get; set; }

        public InvalescoEntity()
        {
        }

        public InvalescoEntity(string firstName)
        {
            EntityName = new Name(firstName);
        }

        public InvalescoEntity(string firstName, string secondName = "", string lastName = "")
        {
            EntityName = new Name(firstName, secondName, lastName);
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.Append(EntityName.ToString())
                         .Append(" ")
                         .Append(Description);

            return stringBuilder.ToString();
        }

    }
}
