﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Invalesco.Core.Model.Common
{
    public class InvalescoImage
    {
        [Key]
        public long Id { get; set; }
        public long InvalescoEntityId { get; set; }

        [Column(TypeName = "image")]
        public byte[] Image { get; set; }
    }
}
