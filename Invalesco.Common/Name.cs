﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invalesco.Common
{
    public class Name
    {
        public InvalescoEntity Entity { get; set; }

        public string ExternalName { get; set; }
        public string InternalName { get; set; }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.Append(ExternalName)
                         .Append(InternalName);

            return stringBuilder.ToString();
        }
    }
}
