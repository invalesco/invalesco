﻿using Invalesco.Application;
using Invalesco.Application.Dto.User;
using Invalesco.Application.Factory;
using Invalesco.Application.Service.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Invalesco.Controllers.User
{
    public class UserController : Controller
    {
        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(UserDto dto)
        {
            IUserService service = InvalescoFactory.GetService<IUserService>();
            service.Register(dto);

            return Login(dto);
        }

        [HttpPost]
        public ActionResult Login(UserDto dto)
        {
            try
            {
                IUserService service = InvalescoFactory.GetService<IUserService>();
                dto = service.Login(dto);
                dto.Password = "";
                dto.ConfirmPassword = "";
                Session["LoggedInUser"] = dto;
            }
            catch (AuthenticationException ae)
            {
                ae.GetType();
                return View(dto);
            }

            return View("Index");
        }

        public ActionResult Logout()
        {
            Session["LoggedInUser"] = null;
            return View("Index");
        }

    }
}
