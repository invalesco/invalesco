﻿using Invalesco.Application.Factory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Invalesco.Controllers.Common
{
    public class InvalescoController : Controller
    {
        protected override void OnException(ExceptionContext filterContext)
        {
            StringBuilder exceptionStringBuilder = new StringBuilder();
            exceptionStringBuilder.Append(filterContext.Exception.ToString());
            exceptionStringBuilder.Append(filterContext.Exception.StackTrace.ToString());
            InvalescoFactory.GetLogger(filterContext.Exception.TargetSite.DeclaringType).Error(exceptionStringBuilder.ToString());
        }
    }
}
