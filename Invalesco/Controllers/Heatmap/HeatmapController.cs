﻿using Invalesco.Application.Dto.Exercise;
using Invalesco.Application.Dto.Program;
using Invalesco.Application.Factory;
using Invalesco.Application.Service.Exercise;
using Invalesco.Application.Service.Program;
using Invalesco.Controllers.Common;
using System.Web.Mvc;

namespace Invalesco.Controllers.Heatmap
{
    public class HeatmapController : InvalescoController
    {
        public ActionResult Heatmap()
        {
            return View();
        }

        public JsonResult GetProgrammableExercises()
        {
            IProgramService service = InvalescoFactory.GetService<IProgramService>();

            return Json(service.GetProgrammableExercises(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetProgrammedExercises(long programId)
        {
            IProgramService service = InvalescoFactory.GetService<IProgramService>();

            return Json(service.GetProgrammedExercises(programId), JsonRequestBehavior.AllowGet);
        }

        public JsonResult AddExerciseToProgram(ProgrammedExerciseDto programmedExerciseDto)
        {
            IProgramService service = InvalescoFactory.GetService<IProgramService>();
            service.AddExerciseToProgram(programmedExerciseDto);

            return Json(programmedExerciseDto, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RemoveExerciseFromProgram(ProgrammedExerciseDto programmedExerciseDto)
        {
            IProgramService service = InvalescoFactory.GetService<IProgramService>();
            service.RemoveExerciseFromProgram(programmedExerciseDto);

            return Json(new { Status = true }, JsonRequestBehavior.AllowGet);
        }
    }
}
