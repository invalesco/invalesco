﻿using Invalesco.Application.Factory;

namespace Invalesco
{
    public class InvalescoFactoryInitializer
    {
        public static void Initialize()
        {
            InvalescoFactory.Initialize();
        }
    }
}