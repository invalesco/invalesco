# README #

## Quick summary  
This repository stores and versions the Invalesco application.  
Invalesco is written with C#, using MVC4 and Entity Framework 6.

## How do I get set up? ###

### Summary of set up  
1. Clone the repository using *git clone*  
1. Open the *Invalesco.sln* file with Visual Studio 2012  
1. Right click the *Invalesco.Web* project and click *Debug -> Start New Instance*   

### Configuration
At this stage in the project does not require extensive configuration. The areas below many be expanded upon though as more functionality is added to the system  
1. Database configuration  
At this stage in the project no database configuration is required  
1. Logging configuration  
Invalesco uses the [ log4net logging library ](http://logging.apache.org/log4net/) 
Logging is stored relative to the project in a directory named 'logs'.
At this stage in the project no logging configuration is required but you can disable appenders from within Web.config  

### Dependencies
Various package dependencies exist, these are currently stored and provided directly through the repository

### Contribution guidelines ###

* Writing tests - **TODO**
> Unit
> Integration
* Code review - **TODO**

### Who do I talk to? ###

* Repo owner or admin
james.aaron.devlin@gmail.com
* Other community or team contact