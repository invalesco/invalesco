﻿
namespace Invalesco.Application
{
    public enum DbError
    {
        Invalid = 0,
        RecordDidNotExist = 1,
        RecordWasUpdatedBySomeoneElse = 2
    }

    public enum UserError
    {
        Invalid = 0,
        PasswordDidNotMatch = 1
    }
}
