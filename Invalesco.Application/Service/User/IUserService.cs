﻿using Invalesco.Application.Dto.User;
using Invalesco.Application.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using U = Invalesco.Core.Model.User;

namespace Invalesco.Application.Service.User
{
    public interface IUserService : IService<U.User, UserDto>
    {
        UserDto Register(UserDto dto);
        UserDto Login(UserDto dto);
        void ChangePassword(UserDto dto);
        void ForgotPassword(long id);
    }
}
