﻿using Invalesco.Application.Dto.User;
using Invalesco.Application.Factory;
using Invalesco.Application.Mapper.User;
using Invalesco.Application.Repository.User;
using Invalesco.Application.Service.Common;
using Invalesco.Application.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using U = Invalesco.Core.Model.User;

namespace Invalesco.Application.Service.User
{
    public class UserService : InvalescoService<U.User, UserDto, IUserRepository>, IUserService
    {
        public UserService()
            : base()
        {
            this.Mapper = new UserMapper();
        }

        public UserDto Register(UserDto dto)
        {
            AESEncryptorDecryptor encryptorDescrytor = new AESEncryptorDecryptor();
            dto.Password = dto.ConfirmPassword = encryptorDescrytor.Encrypt(dto.Password);

            Create(dto);
            return Mapper.MappedDto;
        }

        public UserDto Login(UserDto dto)
        {
            AESEncryptorDecryptor encryptorDecryptor = new AESEncryptorDecryptor();
            U.User user = Repository.FindByName(dto.Username);

            if (encryptorDecryptor.Decrypt(user.Password) == dto.Password)
            {
                Mapper.MapDto(user);
                return Mapper.MappedDto;
            }
            else
                throw new AuthenticationException();
        }

        public void ChangePassword(UserDto dto)
        {
            Update(dto);
        }

        //TODO INV-60
        public void ForgotPassword(long id)
        {
            throw new NotImplementedException();
        }

        public override void SetRepository()
        {
            Repository = InvalescoFactory.GetRepository<IUserRepository>();
        }
    }
}
