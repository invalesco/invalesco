﻿using Invalesco.Application.Context;
using Invalesco.Application.Dto;
using Invalesco.Application.Mapper;
using Invalesco.Core.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invalesco.Application.Service
{
    public abstract class InvalescoService<Entity, Dto, Mapper>
        where Entity : InvalescoEntity
        where Dto : DataTransferObject
        where Mapper : DtoMapper<Entity, Dto>
    {
        public virtual void Create(Dto dto)
        {
            using (InvalescoDbContext context = new InvalescoDbContext())
            {
                Entity entity = context
            }
        }

        public virtual Dto FindById(long id)
        {
            throw new NotImplementedException();
        }

        public virtual IList<Dto> List()
        {
            throw new NotImplementedException();
        }

        public virtual IList<Dto> FilterByName(string name)
        {
            throw new NotImplementedException();
        }

        public virtual IList<Dto> FilterByDescription(string description)
        {
            throw new NotImplementedException();
        }

        public virtual void Update(Dto dto)
        {
            throw new NotImplementedException();
        }

        public virtual void Delete(long id)
        {
            throw new NotImplementedException();
        }

        protected virtual bool ValidateTicks(DataTransferObject dto, InvalescoEntity entity)
        {
            bool areTicksValid = true;

            if (dto.Ticks != entity.UpdatedDate.Ticks)
                areTicksValid = false;

            return areTicksValid;
        }
    }
}
