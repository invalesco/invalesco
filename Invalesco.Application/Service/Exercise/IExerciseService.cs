﻿using Invalesco.Application.Dto.Exercise;
using Invalesco.Application.Service.Common;
using System.Collections.Generic;
using E = Invalesco.Core.Model.Exercise;

namespace Invalesco.Application.Service.Exercise
{
    public interface IExerciseService : IService<E.Exercise, ExerciseDto>
    {
        IList<ExerciseDto> GetExercisesByMuscleGroup(long muscleGroupId);
        IList<ExerciseDto> GetExercisesByMuscle(long muscleId);
        IList<ExerciseDto> GetExercisesByProgram(long programId);
    }
}
