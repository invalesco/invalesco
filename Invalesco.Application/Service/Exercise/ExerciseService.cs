﻿using Invalesco.Application.Context;
using Invalesco.Application.Dto.Exercise;
using Invalesco.Application.Mapper.Exercise;
using Invalesco.Application.Repository.Exercise;
using Invalesco.Application.Service.Common;
using System.Collections.Generic;
using E = Invalesco.Core.Model.Exercise;
using System.Linq;
using Invalesco.Application.Factory;

namespace Invalesco.Application.Service.Exercise
{
    public class ExerciseService : InvalescoService<E.Exercise, ExerciseDto, IExerciseRepository>, IExerciseService
    {
        public ExerciseService()
            : base()
        {
            this.Mapper = new ExerciseMapper();
        }

        public IList<ExerciseDto> GetExercisesByMuscleGroup(long muscleGroupId)
        {
            IList<E.Exercise> exercises = this.Repository.FindExercisesByMuscleGroup(muscleGroupId);

            return MapDtos(exercises);
        }

        public IList<ExerciseDto> GetExercisesByMuscle(long muscleId)
        {
            IList<E.Exercise> exercises = this.Repository.FindExercisesByMuscle(muscleId);

            return MapDtos(exercises);
        }

        public IList<ExerciseDto> GetExercisesByProgram(long programId)
        {
            IList<E.Exercise> exercises = this.Repository.FindExercisesByProgram(programId);

            return MapDtos(exercises);
        }

        public override void SetRepository()
        {
            Repository = InvalescoFactory.GetRepository<IExerciseRepository>();
        }
    }
}