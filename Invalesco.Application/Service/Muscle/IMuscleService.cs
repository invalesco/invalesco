﻿
using Invalesco.Application.Service.Common;
using Invalesco.Application.Dto.Muscle;
using M = Invalesco.Core.Model.Muscle;
using System.Collections.Generic;

namespace Invalesco.Application.Service.Muscle
{
    public interface IMuscleService : IService<M.Muscle, MuscleDto>
    {
        IList<MuscleDto> GetMusclesByExercise(long exerciseId);
    }
}
