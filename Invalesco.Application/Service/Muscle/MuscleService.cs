﻿using Invalesco.Application.Dto.Muscle;
using Invalesco.Application.Factory;
using Invalesco.Application.Mapper.Muscle;
using Invalesco.Application.Repository.Muscle;
using Invalesco.Application.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using M = Invalesco.Core.Model.Muscle;

namespace Invalesco.Application.Service.Muscle
{
    public class MuscleService : InvalescoService<M.Muscle, MuscleDto, IMuscleRepository>, IMuscleService
    {
        public MuscleService()
            : base()
        {
            this.Mapper = new MuscleMapper();
        }

        public IList<MuscleDto> GetMusclesByExercise(long exerciseId)
        {
            IList<MuscleDto> muscleDtos = new List<MuscleDto>();
            IList<M.Muscle> muscles = this.Repository.FindMusclesByExercise(exerciseId);

            foreach (M.Muscle muscle in muscles)
            {
                Mapper.MapDto(muscle);
                muscleDtos.Add(Mapper.MappedDto);
            }

            return muscleDtos;
        }

        public override void SetRepository()
        {
            Repository = InvalescoFactory.GetRepository<IMuscleRepository>();
        }
    }
}
