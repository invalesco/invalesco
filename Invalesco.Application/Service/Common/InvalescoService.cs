﻿using Invalesco.Application.Dto.Common;
using Invalesco.Application.Factory;
using Invalesco.Application.Mapper.Common;
using Invalesco.Application.Repository.Common;
using Invalesco.Application.Utility;
using Invalesco.Core.Model.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Invalesco.Application.Service.Common
{
    public abstract class InvalescoService<Entity, Dto, Repo> : IService<Entity, Dto>
        where Entity : InvalescoEntity, new()
        where Dto : DataTransferObject, new()
        where Repo : IRepository<Entity>
    {
        protected DtoMapper<Entity, Dto> Mapper { get; set; }
        protected Repo Repository { get; set; }

        public InvalescoService()
        {
            SetRepository();
        }

        public abstract void SetRepository();

        public virtual IList<Dto> List()
        {
            IList<Entity> entities = Repository.List().ToList();

            return MapDtos(entities);
        }

        public virtual Dto Create(Dto dto)
        {
            DataAnnotationsValidator validator = new DataAnnotationsValidator();

            try
            {
                validator.TryValidate(dto);
            }
            catch (ValidationException validationException)
            {
                InvalescoFactory.GetLogger(this.GetType()).Warn(validationException);
                throw validationException;
            }

            Mapper.IsUpdating = false;
            Mapper.MapEntity(dto);
            Entity entity = Repository.Create(Mapper.MappedEntity);
            Mapper.MapDto(entity);
            return Mapper.MappedDto;
        }

        public virtual Dto Update(Dto dto)
        {
            DataAnnotationsValidator validator = new DataAnnotationsValidator();
            validator.TryValidate(dto);

            Mapper.IsUpdating = true;
            Mapper.MapEntity(dto);
            Entity entity = null;

            if (Mapper.MappedDto.Ticks == Mapper.MappedEntity.UpdatedDate.Value.Ticks)
                entity = Repository.Update(Mapper.MappedEntity);
            else
            {
                Exception concurrencyException = new Exception(DbError.RecordWasUpdatedBySomeoneElse.ToString());
                InvalescoFactory.GetLogger(this.GetType()).Warn(concurrencyException);
            }

            Mapper.MapDto(entity);
            return Mapper.MappedDto;
        }

        public virtual void Delete(long id)
        {
            Repository.Delete(id);
        }

        public virtual void Delete(long[] ids)
        {
            foreach (long id in ids)
                Repository.Delete(id);
        }

        public virtual Dto FindDtoById(long id)
        {
            Mapper.MapDto(Repository.FindById(id));
            return Mapper.MappedDto;
        }

        public virtual Dto FindDtoByName(String name)
        {
            Entity entity = Repository.FindByName(name);

            if(entity == null)
                throw new EntityNotFoundException();

            Mapper.MapDto(entity);
            return Mapper.MappedDto;
        }

        protected virtual IList<Dto> MapDtos(IList<Entity> entities)
        {
            IList<Dto> dtos = new List<Dto>();

            foreach (Entity entity in entities)
            {
                Mapper.MapDto(entity);
                dtos.Add(Mapper.MappedDto);
            }

            return dtos;
        }
    }
}
