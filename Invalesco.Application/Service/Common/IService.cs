﻿using Invalesco.Application.Dto.Common;
using System;
using System.Collections.Generic;

namespace Invalesco.Application.Service.Common
{
    public interface IService<Entity, Dto>
    {
        IList<Dto> List();

        Dto Create(Dto dto);
        Dto Update(Dto dto);
        void Delete(long id);
        void Delete(long[] ids);
    }
}
