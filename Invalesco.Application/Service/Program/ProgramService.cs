﻿using Invalesco.Application.Dto.Program;
using Invalesco.Application.Factory;
using Invalesco.Application.Repository.Exercise;
using Invalesco.Application.Repository.Program;
using Invalesco.Application.Service.Common;
using System;
using System.Linq;

using P = Invalesco.Core.Model.Program;
using E = Invalesco.Core.Model.Exercise;
using Invalesco.Core.Model.Program;
using System.Collections.Generic;
using Invalesco.Application.Mapper.Program;

namespace Invalesco.Application.Service.Program
{
    public class ProgramService : InvalescoService<P.Program, ProgramDto, IProgramRepository>, IProgramService
    {
        public ProgramService()
            : base()
        {
            this.Mapper = new ProgramMapper();
        }

        public void AddExerciseToProgram(ProgrammedExerciseDto programmedExerciseDto)
        {
            P.Program program = Repository.FindById(programmedExerciseDto.ProgramId);
            E.Exercise exercise = InvalescoFactory.GetRepository<IExerciseRepository>().FindById(programmedExerciseDto.ExerciseId);

            ProgrammedExercise programmedExercise = new ProgrammedExercise()
            {
                Program = program,
                Exercise = exercise
            };

            program.ProgrammedExercises.Add(programmedExercise);

            Repository.Update(program);
            programmedExerciseDto.Id = programmedExercise.Id;
        }

        public void RemoveExerciseFromProgram(ProgrammedExerciseDto programmedExerciseDto)
        {
            P.Program program = Repository.FindById(programmedExerciseDto.ProgramId);
            ProgrammedExercise programmedExercise = program.ProgrammedExercises.FirstOrDefault(x => x.Id == programmedExerciseDto.Id);

            if(programmedExercise == null)
                throw new Exception(DbError.RecordDidNotExist.ToString());

            program.ProgrammedExercises.Remove(programmedExercise);

            Repository.Update(program);
        }

        public IList<ProgrammedExerciseDto> GetProgrammableExercises()
        {
            return (from e in Repository.List()
                    select new ProgrammedExerciseDto {
                        ExerciseId = e.Id,
                        ExerciseName = e.EntityName.FirstName
                    }).ToList();
        }

        public IList<ProgrammedExerciseDto> GetProgrammedExercises(long programId)
        {
            P.Program program = Repository.FindById(programId);

            return (from e in program.ProgrammedExercises
                    select new ProgrammedExerciseDto {
                        Id = e.Id,
                        ExerciseId = e.Exercise.Id,
                        ExerciseName = e.Exercise.EntityName.FirstName,
                        ProgramId = program.Id
                    }).ToList();
        }

        public override void SetRepository()
        {
            Repository = InvalescoFactory.GetRepository<IProgramRepository>();
        }
    }
}
