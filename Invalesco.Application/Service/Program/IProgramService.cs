﻿using Invalesco.Application.Dto.Exercise;
using Invalesco.Application.Dto.Program;
using Invalesco.Application.Service.Common;
using System.Collections.Generic;
using P = Invalesco.Core.Model.Program;

namespace Invalesco.Application.Service.Program
{
    public interface IProgramService : IService<P.Program, ProgramDto>
    {
        IList<ProgrammedExerciseDto> GetProgrammableExercises();
        void AddExerciseToProgram(ProgrammedExerciseDto programmedExerciseDto);
        void RemoveExerciseFromProgram(ProgrammedExerciseDto programmedExerciseDto);
        IList<ProgrammedExerciseDto> GetProgrammedExercises(long programId);
    }
}
