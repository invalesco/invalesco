﻿using Invalesco.Application.Dto.Common;
using Invalesco.Application.Repository.Common;
using Invalesco.Application.Repository.Exercise;
using Invalesco.Application.Repository.Muscle;
using Invalesco.Application.Repository.Program;
using Invalesco.Application.Repository.User;
using Invalesco.Application.Service.Common;
using Invalesco.Application.Service.Exercise;
using Invalesco.Application.Service.Muscle;
using Invalesco.Application.Service.Program;
using Invalesco.Application.Service.User;
using Invalesco.Core.Model.Common;
using log4net;
using log4net.Config;
using SimpleInjector;
using System;

namespace Invalesco.Application.Factory
{

    /// <summary>
    /// Access to facilities such as logging and container
    /// </summary>
    public static class InvalescoFactory
    {

        private static Container Container;

        public static ILog GetLogger(Type entityType)
        {
            return LogManager.GetLogger(entityType.FullName);
        }

        public static void Initialize()
        {
            InitializeContainer();
            InitializeLogger();
        }

        private static void InitializeContainer()
        {
            Container = new Container();

            Container.Register<IExerciseService, ExerciseService>();
            Container.Register<IExerciseRepository, ExerciseRepository>();

            Container.Register<IMuscleService, MuscleService>();
            Container.Register<IMuscleRepository, MuscleRepository>();

            Container.Register<IProgramService, ProgramService>();
            Container.Register<IProgramRepository, ProgramRepository>();

            Container.Register<IUserService, UserService>();
            Container.Register<IUserRepository, UserRepository>();
        }

        private static void InitializeLogger()
        {
            XmlConfigurator.Configure();
        }

        public static R GetRepository<R>() where R : class
        {
            return Container.GetInstance<R>();
        }
        
        public static S GetService<S>() where S : class
        {
            return Container.GetInstance<S>();
        }
    }
}
