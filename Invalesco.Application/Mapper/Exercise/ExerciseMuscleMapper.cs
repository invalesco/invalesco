﻿using Invalesco.Application.Context;
using Invalesco.Application.Dto.Exercise;
using Invalesco.Application.Mapper.Common;
using Invalesco.Core.Model.Exercise;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invalesco.Application.Mapper.Exercise
{
    public class ExerciseMuscleMapper : DtoMapper<ExerciseMuscle, ExerciseMuscleDto>
    {
        protected override void MapExtendedDtoProperties()
        {
            if (IsUpdating)
            {
                this.MappedDto.MuscleId = this.MappedEntity.Muscle.Id;
                this.MappedDto.ExerciseId = this.MappedEntity.Exercise.Id;
            }

            this.MappedDto.MuscleInvolvement = this.MappedEntity.MuscleInvolvement;
        }

        protected override void MapExtendedEntityProperties()
        {
            using (var context = new InvalescoDbContext())
            {
                this.MappedEntity.Muscle = context.Muscles.FirstOrDefault(x => x.Id == MappedDto.MuscleId);
                this.MappedEntity.Exercise = context.Exercises.FirstOrDefault(x => x.Id == MappedDto.ExerciseId);
            }

            this.MappedEntity.MuscleInvolvement = this.MappedDto.MuscleInvolvement;
        }
    }
}
