﻿using Invalesco.Application.Dto.Exercise;
using Invalesco.Application.Mapper.Common;
using Invalesco.Core.Model.Exercise;
using E = Invalesco.Core.Model.Exercise;

namespace Invalesco.Application.Mapper.Exercise
{
    public class ExerciseMapper : DtoMapper<E.Exercise, ExerciseDto>
    {

        protected override void MapExtendedDtoProperties()
        {
            ExerciseMuscleMapper exerciseMuscleMapper = new ExerciseMuscleMapper();

            this.MappedDto.ExerciseMuscleDtos.Clear();
            foreach (ExerciseMuscle exerciseMuscle in this.MappedEntity.ExerciseMuscles)
            {
                exerciseMuscleMapper.MapDto(exerciseMuscle);
            }
        }

        protected override void MapExtendedEntityProperties()
        {
            
        }
    }
}
