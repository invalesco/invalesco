﻿using Invalesco.Application.Dto.Program;
using Invalesco.Application.Mapper.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using P = Invalesco.Core.Model.Program;

namespace Invalesco.Application.Mapper.Program
{
    public class ProgramMapper : DtoMapper<P.Program, ProgramDto>
    {
        protected override void MapExtendedDtoProperties() { }
        protected override void MapExtendedEntityProperties() { }
    }
}
