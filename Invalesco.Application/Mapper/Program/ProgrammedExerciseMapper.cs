﻿using Invalesco.Application.Dto.Program;
using Invalesco.Application.Mapper.Common;
using Invalesco.Core.Model.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invalesco.Application.Mapper.Program
{
    public class ProgrammedExerciseMapper : DtoMapper<ProgrammedExercise, ProgrammedExerciseDto>
    {
        protected override void MapExtendedDtoProperties()
        {
            this.MappedDto.ExerciseId = this.MappedEntity.Exercise.Id;
            this.MappedDto.ProgramId = this.MappedEntity.Program.Id;
        }

        protected override void MapExtendedEntityProperties()
        {
            
        }
    }
}
