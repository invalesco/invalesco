﻿using System.Linq;

using Invalesco.Application.Dto.Common;
using Invalesco.Core.Model.Common;

namespace Invalesco.Application.Mapper.Common
{
    public abstract class DtoMapper<Entity, Dto> 
        where Entity : InvalescoEntity, new()
        where Dto : DataTransferObject, new()
    {

        public virtual Entity MappedEntity { get; set; }
        public virtual Dto MappedDto { get; set; }

        public bool IsUpdating { get; set; }

        public virtual void MapDto(Entity entity)
        {
            this.MappedEntity = entity;
            this.MappedDto = new Dto();
            IsUpdating = true;
            MapEntityToDto();
        }

        public virtual void MapEntity(Dto dto)
        {
            this.MappedDto = dto;
            this.MappedEntity = new Entity();
            MapDtoToEntity();
        }

        private void MapEntityToDto()
        {
            this.MappedDto.Id = this.MappedEntity.Id;
            this.MappedDto.UpdateDate = this.MappedEntity.UpdatedDate;
            this.MappedDto.Comments = this.MappedEntity.Comments;
            this.MappedDto.Description = this.MappedEntity.Description;

            if (this.MappedDto.GetType() != typeof(Invalesco.Application.Dto.Common.NameDto) && this.MappedEntity.EntityName != null)
            {
                NameMapper nameMapper = new NameMapper()
                {
                    MappedEntity = this.MappedEntity.EntityName,
                    MappedDto = new NameDto(this.MappedEntity.EntityName.FirstName, this.MappedEntity.EntityName.SecondName)
                };

                nameMapper.MapEntityToDto();
                this.MappedDto.FirstName = nameMapper.MappedDto.FirstName;
                this.MappedDto.SecondName = nameMapper.MappedDto.SecondName;
                this.MappedDto.LastName = nameMapper.MappedDto.LastName;
            }

            MapExtendedDtoProperties();
        }

        private void MapDtoToEntity()
        {
            if (!IsUpdating)
            {
                this.MappedEntity.EntityName = new Name();
            }

            this.MappedEntity.Comments = this.MappedDto.Comments;
            this.MappedEntity.Description = this.MappedDto.Description;
            this.MappedEntity.EntityName.FirstName = this.MappedDto.FirstName;
            this.MappedEntity.EntityName.SecondName = this.MappedDto.SecondName;

            if (this.MappedEntity.GetType() != typeof(Invalesco.Core.Model.Common.Name))
            {
                NameMapper nameMapper = new NameMapper();

                nameMapper.MappedEntity = this.MappedEntity.EntityName;
                nameMapper.MappedDto = new NameDto(this.MappedDto.FirstName, this.MappedDto.SecondName);

                nameMapper.MapDtoToEntity();

                //TODO Re-introduce other names to DataTransferObject class

                //foreach (NameDto nameDto in this.MappedDto.OtherNames)
                //{
                //    nameMapper.MappedEntity = this.MappedEntity.OtherNames.FirstOrDefault(x => x.Id == nameDto.Id);
                //    nameMapper.MappedDto = nameDto;

                //    nameMapper.MapEntityToDto();
                //}
            }

            MapExtendedEntityProperties();
        }

        protected abstract void MapExtendedDtoProperties();

        //TODO Evaluate usefulness of this method
        protected abstract void MapExtendedEntityProperties();
    }
}
