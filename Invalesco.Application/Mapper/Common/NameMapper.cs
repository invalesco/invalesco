﻿using Invalesco.Application.Dto.Common;
using Invalesco.Core.Model.Common;

namespace Invalesco.Application.Mapper.Common
{
    public class NameMapper : DtoMapper<Name, NameDto>
    {
        protected override void MapExtendedDtoProperties()
        {
            this.MappedDto.FirstName = this.MappedEntity.FirstName;
            this.MappedDto.SecondName = this.MappedEntity.SecondName;
        }

        protected override void MapExtendedEntityProperties()
        {
            this.MappedEntity.FirstName = this.MappedDto.FirstName;
            this.MappedEntity.SecondName = this.MappedDto.SecondName;
        }
    }
}
