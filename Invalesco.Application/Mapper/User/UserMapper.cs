﻿using Invalesco.Application.Dto.User;
using Invalesco.Application.Mapper.Common;
using Invalesco.Application.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using U = Invalesco.Core.Model.User;

namespace Invalesco.Application.Mapper.User
{
    public class UserMapper : DtoMapper<U.User, UserDto>
    {
        protected override void MapExtendedDtoProperties() 
        {
            MappedDto.EmailAddress = MappedEntity.EmailAddress;
            MappedDto.Username = MappedEntity.Username;
            MappedDto.Password = MappedEntity.Password;
        }

        protected override void MapExtendedEntityProperties() 
        {
            MappedEntity.EmailAddress = MappedDto.EmailAddress;
            MappedEntity.Username = MappedDto.Username;
            MappedEntity.Password = MappedDto.Password;
        }
    }
}
