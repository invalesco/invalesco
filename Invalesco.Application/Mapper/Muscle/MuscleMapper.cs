﻿using Invalesco.Application.Dto.Muscle;
using Invalesco.Application.Mapper.Common;
using M = Invalesco.Core.Model.Muscle;

namespace Invalesco.Application.Mapper.Muscle
{
    public class MuscleMapper : DtoMapper<M.Muscle, MuscleDto>
    {
        protected override void MapExtendedDtoProperties()
        {
            MuscleGroupMapper muscleGroupMapper = new MuscleGroupMapper();
            muscleGroupMapper.MapDto(MappedEntity.MuscleGroup);

            MappedDto.MuscleGroupId = muscleGroupMapper.MappedDto.Id;
        }

        protected override void MapExtendedEntityProperties()
        {
            
        }
    }
}
