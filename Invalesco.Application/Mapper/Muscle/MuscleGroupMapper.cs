﻿using Invalesco.Application.Dto.Muscle;
using Invalesco.Application.Mapper.Common;
using M = Invalesco.Core.Model.Muscle;

namespace Invalesco.Application.Mapper.Muscle
{
    public class MuscleGroupMapper : DtoMapper<M.MuscleGroup, MuscleGroupDto>
    {
        protected override void MapExtendedDtoProperties()
        {

        }

        protected override void MapExtendedEntityProperties()
        {
            
        }
    }
}
