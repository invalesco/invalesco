﻿using Invalesco.Application.Dto.Common;
using System;

namespace Invalesco.Application.Dto.Program
{
    public class ProgrammedExerciseDto : DataTransferObject
    {
        public String ExerciseName { get; set; }
        public long ExerciseId { get; set; }
        public long ProgramId { get; set; }
    }
}
