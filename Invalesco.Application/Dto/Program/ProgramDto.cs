﻿using Invalesco.Application.Dto.Common;
using System.Collections.Generic;

namespace Invalesco.Application.Dto.Program
{
    public class ProgramDto : DataTransferObject
    {
        public IList<ProgrammedExerciseDto> ProgrammedExerciseDtos { get; set; }
    }
}
