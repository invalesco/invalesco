﻿using Invalesco.Application.Dto.Common;
using System.Collections.Generic;

namespace Invalesco.Application.Dto.Muscle
{
    public class MuscleGroupDto : DataTransferObject
    {
        public IList<MuscleDto> Muscles { get; set; }
    }
}
