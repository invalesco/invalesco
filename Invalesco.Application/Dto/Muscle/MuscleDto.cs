﻿using Invalesco.Application.Dto.Common;

namespace Invalesco.Application.Dto.Muscle
{
    public class MuscleDto : DataTransferObject
    {
        public long MuscleGroupId { get; set; }
    }
}
