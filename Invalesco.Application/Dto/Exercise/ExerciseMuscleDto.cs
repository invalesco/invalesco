﻿using Invalesco.Application.Dto.Common;
using Invalesco.Core.Model;

namespace Invalesco.Application.Dto.Exercise
{
    /// <summary>
    /// Data transfer object to support the ExerciseMuscle domain model
    /// </summary>
    public class ExerciseMuscleDto : DataTransferObject
    {
        public long ExerciseId { get; set; }
        public long MuscleId { get; set; }

        public virtual MuscleInvolvement MuscleInvolvement { get; set; }
    }
}
