﻿using Invalesco.Application.Dto.Common;
using System.Collections.Generic;

namespace Invalesco.Application.Dto.Exercise
{
    public class ExerciseDto : DataTransferObject
    {
        public virtual IList<ExerciseMuscleDto> ExerciseMuscleDtos { get; set; }

        public ExerciseDto()
        {
            ExerciseMuscleDtos = new List<ExerciseMuscleDto>();
        }
    }
}
