﻿using Invalesco.Application.Dto.Common;
using System;
using System.ComponentModel.DataAnnotations;

namespace Invalesco.Application.Dto.User
{
    public class UserDto : DataTransferObject
    {
        [Display(Name = "User name")]
        public virtual String Username { get; set; }

        [DataType(DataType.EmailAddress)]
        [Display(Name = "E-mail")]
        [EmailAddress]
        [Required]
        public virtual String EmailAddress { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        public virtual String Password { get; set; }

        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        public virtual String ConfirmPassword { get; set; }
    }
}
