﻿
using System;
namespace Invalesco.Application.Dto.Common
{
    public class NameDto : DataTransferObject
    {
        public NameDto()
        { }

        public NameDto(String externalName)
        {
            FirstName = externalName;
        }

        public NameDto(String externalName, String internalName)
        {
            FirstName = externalName;
            SecondName = internalName;
        }
    }
}
