﻿using System;
using System.Collections.Generic;

namespace Invalesco.Application.Dto.Common
{
    public class DataTransferObject
    {
        public DataTransferObject()
        { }

        public virtual long Id { get; set; }
        public virtual long Ticks { get; set; }
        public virtual DateTime? UpdateDate { get; set; }
        public virtual String FirstName { get; set; }
        public virtual String SecondName { get; set; }
        public virtual String LastName { get; set; }
        public virtual String Description { get; set; }
        public virtual String Comments { get; set; }
    }
}
