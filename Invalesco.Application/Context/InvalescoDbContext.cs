﻿using Invalesco.Core.Model.Exercise;
using Invalesco.Core.Model.Muscle;
using Invalesco.Core.Model.Program;
using Invalesco.Core.Model.User;
using System.Data.Entity;

namespace Invalesco.Application.Context
{
    public class InvalescoDbContext : DbContext
    {
        public InvalescoDbContext()
            : base("InvalescoDb")
        {
            Database.SetInitializer(new InvalescoDbContextInitializer());
        }

        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Exercise> Exercises { get; set; }
        public virtual DbSet<ExerciseMuscle> ExerciseMuscles { get; set; }
        public virtual DbSet<Muscle> Muscles { get; set; }
        public virtual DbSet<MuscleGroup> MuscleGroups { get; set; }
        public virtual DbSet<Program> Programs { get; set; }
        public virtual DbSet<ProgrammedExercise> ProgrammedExercises { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().ToTable("Users");
            modelBuilder.Entity<Exercise>().ToTable("Exercises");
            modelBuilder.Entity<ExerciseMuscle>().ToTable("ExerciseMuscles");
            modelBuilder.Entity<Muscle>().ToTable("Muscles");
            modelBuilder.Entity<MuscleGroup>().ToTable("MuscleGroups");
            modelBuilder.Entity<Program>().ToTable("Programs");
            modelBuilder.Entity<ProgrammedExercise>().ToTable("ProgrammedExercises");
        }
    }
}
