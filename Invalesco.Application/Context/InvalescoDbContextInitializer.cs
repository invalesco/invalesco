﻿using Invalesco.Core.Model;
using Invalesco.Core.Model.Common;
using Invalesco.Core.Model.Exercise;
using Invalesco.Core.Model.Muscle;
using Invalesco.Core.Model.Program;
using System.Data.Entity;

namespace Invalesco.Application.Context
{
    public class InvalescoDbContextInitializer : DropCreateDatabaseAlways<InvalescoDbContext>
    {
        protected override void Seed(InvalescoDbContext context)
        {
            Muscle gluteusMaximus = new Muscle()
            {
                EntityName = new Name("Gluteus Maximus"),
                Description = "",
                MuscleName = MuscleName.GluteusMaximus
            };

            Muscle rectusFemoris = new Muscle()
            {
                EntityName = new Name("Rectus Femoris"),
                Description = "",
                MuscleName = MuscleName.RectusFemoris
            };

            Muscle vastusIntermedius = new Muscle()
            {
                EntityName = new Name("Vastus Intermedius"),
                Description = "",
                MuscleName = MuscleName.VastusIntermedius
            };

            Muscle vastusLateralis = new Muscle()
            {
                EntityName = new Name("Vastus Lateralis"),
                Description = "",
                MuscleName = MuscleName.VastusLaterialis
            };

            Muscle vastusMedialis = new Muscle()
            {
                EntityName = new Name("Vastus Medialis"),
                Description = "",
                MuscleName = MuscleName.VastusMedialis
            };

            context.Muscles.Add(rectusFemoris);
            context.Muscles.Add(vastusIntermedius);
            context.Muscles.Add(vastusLateralis);
            context.Muscles.Add(vastusMedialis);
            context.Muscles.Add(gluteusMaximus);

            MuscleGroup quadriceps = new MuscleGroup()
            {
                MuscleGroupName = MuscleGroupName.Quadriceps
            };

            quadriceps.Muscles.Add(rectusFemoris);
            quadriceps.Muscles.Add(vastusIntermedius);
            quadriceps.Muscles.Add(vastusLateralis);
            quadriceps.Muscles.Add(vastusMedialis);

            MuscleGroup gluteals = new MuscleGroup()
            {
                MuscleGroupName = MuscleGroupName.Gluteals
            };

            gluteals.Muscles.Add(gluteusMaximus);

            context.MuscleGroups.Add(quadriceps);
            context.MuscleGroups.Add(gluteals);

            Exercise squat = new Exercise()
            {
                EntityName = new Name("Squat"),
                Description = "In strength training, the squat is a compound, full body exercise that trains primarily the muscles of the thighs, hips and buttocks, quads, hamstrings, as well as strengthening the bones, ligaments and insertion of the tendons throughout the lower body"
            };

            Exercise deadlift = new Exercise()
            {
                EntityName = new Name("Deadlift"),
                Description = "The deadlift is a weight training exercise in which a loaded barbell is lifted off the ground to the hips, then lowered back to the ground."
            };

            context.Exercises.Add(squat);
            context.Exercises.Add(deadlift);

            ExerciseMuscle squatRectusFemoris = new ExerciseMuscle()
            {
                Exercise = squat,
                Muscle = rectusFemoris,
                MuscleInvolvement = MuscleInvolvement.Synergist
            };

            ExerciseMuscle squatVastusIntermedius = new ExerciseMuscle()
            {
                Exercise = squat,
                Muscle = vastusIntermedius,
                MuscleInvolvement = MuscleInvolvement.Synergist
            };

            ExerciseMuscle squatVastusLateralis = new ExerciseMuscle()
            {
                Exercise = squat,
                Muscle = vastusLateralis,
                MuscleInvolvement = MuscleInvolvement.Synergist
            };

            ExerciseMuscle squatVastusMedialis = new ExerciseMuscle()
            {
                Exercise = squat,
                Muscle = vastusMedialis,
                MuscleInvolvement = MuscleInvolvement.Synergist
            };

            ExerciseMuscle deadliftGluteusMaximus = new ExerciseMuscle()
            {
                Exercise = deadlift,
                Muscle = gluteusMaximus,
                MuscleInvolvement = MuscleInvolvement.Synergist
            };

            context.ExerciseMuscles.Add(squatRectusFemoris);
            context.ExerciseMuscles.Add(squatVastusIntermedius);
            context.ExerciseMuscles.Add(squatVastusLateralis);
            context.ExerciseMuscles.Add(squatVastusMedialis);
            context.ExerciseMuscles.Add(deadliftGluteusMaximus);

            ProgrammedExercise programmedSquat = new ProgrammedExercise()
            {
                Exercise = squat
            };

            ProgrammedExercise programmedDeadlift = new ProgrammedExercise()
            {
                Exercise = deadlift
            };

            context.ProgrammedExercises.Add(programmedSquat);
            context.ProgrammedExercises.Add(programmedDeadlift);

            Program program = new Program()
            {
                EntityName = new Name("Default Program"),
                Description = "The default program",
            };

            program.ProgrammedExercises.Add(programmedSquat);
            program.ProgrammedExercises.Add(programmedDeadlift);

            context.Programs.Add(program);

            base.Seed(context);
        }
    }
}
