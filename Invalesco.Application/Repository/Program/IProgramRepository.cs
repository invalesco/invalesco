﻿using Invalesco.Application.Repository.Common;

using P = Invalesco.Core.Model.Program;
using E = Invalesco.Core.Model.Exercise;

namespace Invalesco.Application.Repository.Program
{
    public interface IProgramRepository : IRepository<P.Program>
    {
    }
}
