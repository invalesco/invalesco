﻿using Invalesco.Application.Context;
using Invalesco.Application.Repository.Common;
using System;
using System.Linq;
using P = Invalesco.Core.Model.Program;
using E = Invalesco.Core.Model.Exercise;
using System.Data.Entity;

namespace Invalesco.Application.Repository.Program
{
    public class ProgramRepository : InvalescoRepository<P.Program>, IProgramRepository
    {
    }
}
