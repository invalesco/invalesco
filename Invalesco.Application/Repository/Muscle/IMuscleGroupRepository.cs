﻿using Invalesco.Application.Repository.Common;
using M = Invalesco.Core.Model.Muscle;

namespace Invalesco.Application.Repository.Muscle
{
    public interface IMuscleGroupRepository : IRepository<M.MuscleGroup>
    {
    }
}
