﻿using Invalesco.Application.Context;
using Invalesco.Application.Repository.Common;
using System.Collections.Generic;
using M = Invalesco.Core.Model.Muscle;
using System.Linq;
using System;
using Invalesco.Application.Factory;
using System.Data.Entity;


namespace Invalesco.Application.Repository.Muscle
{
    public class MuscleRepository : InvalescoRepository<M.Muscle>, IMuscleRepository
    {
        public IList<M.Muscle> FindMusclesByExercise(long exerciseId)
        {
            return Context.ExerciseMuscles.Where(x => x.Exercise.Id == exerciseId)
                                          .Select(y => y.Muscle).ToList();
        }
    }
}
