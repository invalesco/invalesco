﻿using Invalesco.Application.Repository.Common;
using System.Collections.Generic;
using M = Invalesco.Core.Model.Muscle;

namespace Invalesco.Application.Repository.Muscle
{
    public interface IMuscleRepository : IRepository<M.Muscle>
    {
        IList<M.Muscle> FindMusclesByExercise(long exerciseId);
    }
}
