﻿using Invalesco.Application.Context;
using Invalesco.Application.Repository.Common;
using System.Data.Entity;
using M = Invalesco.Core.Model.Muscle;

namespace Invalesco.Application.Repository.Muscle
{
    public class MuscleGroupRepository : InvalescoRepository<M.MuscleGroup>, IMuscleGroupRepository
    {
    }
}
