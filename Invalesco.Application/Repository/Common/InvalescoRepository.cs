﻿using Invalesco.Application.Context;
using Invalesco.Application.Factory;
using Invalesco.Core.Model.Common;
using log4net;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Invalesco.Application.Repository.Common
{
    public abstract class InvalescoRepository<Entity> : IRepository<Entity>, IDisposable
        where Entity : InvalescoEntity
    {
        protected static InvalescoDbContext Context { get; private set; }

        public InvalescoRepository()
        {
            Context = new InvalescoDbContext();
        }

        public virtual Entity Create(Entity entity)
        {
            Context.Set<Entity>().Add(entity);
            Context.SaveChanges();
            return entity;
        }

        public virtual Entity Update(Entity entity)
        {
            Context.Entry(entity).State = EntityState.Modified;
            Context.SaveChanges();
            return entity;
        }

        public virtual void Delete(long id)
        {
            Entity entity = FindById(id);
            Context.Set<Entity>().Remove(entity);
            Context.SaveChanges();
        }

        public virtual IList<Entity> List()
        {
            return Context.Set<Entity>().ToList();
        }

        public virtual Entity FindById(long id)
        {
            Entity entity = Context.Set<Entity>().FirstOrDefault(x => x.Id == id);

            if (entity == null)
                throw new Exception(DbError.RecordDidNotExist.ToString());

            return entity;
        }

        public virtual Entity FindByName(String name)
        {
            Entity entity = Context.Set<Entity>().FirstOrDefault(x => x.EntityName.FirstName == name);
            return entity;
        }

        public virtual IList<Entity> FindManyByName(String name)
        {
            IList<Entity> entities = Context.Set<Entity>().Where(x => x.EntityName.FirstName == name).ToList();
            return entities;
        }

        public void Dispose()
        {
            Context.Dispose();
        }
    }
}
