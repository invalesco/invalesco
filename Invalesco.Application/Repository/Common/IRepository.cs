﻿using System;
using System.Collections.Generic;

namespace Invalesco.Application.Repository.Common
{
    public interface IRepository<Entity>
    {
        Entity Create(Entity entity);
        Entity Update(Entity entity);
        void Delete(long id);

        IList<Entity> List();
        Entity FindById(long id);
        Entity FindByName(String name);
        IList<Entity> FindManyByName(String name);
    }
}
