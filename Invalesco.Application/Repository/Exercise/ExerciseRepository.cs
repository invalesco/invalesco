﻿using Invalesco.Application.Context;
using Invalesco.Application.Repository.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using E = Invalesco.Core.Model.Exercise;

namespace Invalesco.Application.Repository.Exercise
{
    public class ExerciseRepository : InvalescoRepository<E.Exercise>, IExerciseRepository
    {
        public IList<E.Exercise> FindExercisesByMuscleGroup(long muscleGroupId)
        {
            return Context.ExerciseMuscles.Where(x => x.Muscle.MuscleGroup.Id == muscleGroupId)
                                            .Select(y => y.Exercise)
                                            .ToList();
        }

        public IList<E.Exercise> FindExercisesByMuscle(long muscleId)
        {
            return Context.ExerciseMuscles.Where(x => x.Muscle.Id == muscleId)
                                            .Select(y => y.Exercise)
                                            .ToList();
        }

        public IList<E.Exercise> FindExercisesByProgram(long programId)
        {
            return Context.ProgrammedExercises.Where(x => x.Program.Id == programId)
                                              .Select(y => y.Exercise)
                                              .ToList();
        }
    }
}
