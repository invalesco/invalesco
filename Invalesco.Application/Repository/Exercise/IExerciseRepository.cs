﻿using Invalesco.Application.Repository.Common;
using System.Collections.Generic;
using E = Invalesco.Core.Model.Exercise;

namespace Invalesco.Application.Repository.Exercise
{
    public interface IExerciseRepository : IRepository<E.Exercise>
    {
        IList<E.Exercise> FindExercisesByMuscleGroup(long muscleGroupId);
        IList<E.Exercise> FindExercisesByMuscle(long muscleId);
        IList<E.Exercise> FindExercisesByProgram(long programId);
    }
}
