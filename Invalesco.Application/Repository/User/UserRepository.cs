﻿using Invalesco.Application.Repository.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using U = Invalesco.Core.Model.User;

namespace Invalesco.Application.Repository.User
{
    public class UserRepository : InvalescoRepository<U.User>, IUserRepository
    {
    }
}
