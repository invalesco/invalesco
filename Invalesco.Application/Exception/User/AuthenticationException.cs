﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invalesco.Application
{
    public class AuthenticationException : Exception
    {
        public AuthenticationException()
            : base()
        { }

        public AuthenticationException(String message)
            : base(message)
        { }

        public AuthenticationException(String message, Exception inner)
        : base(message, inner) 
        { }

    }
}
