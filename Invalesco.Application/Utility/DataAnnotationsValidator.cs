﻿using System.Text;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Invalesco.Application.Utility
{
    public class DataAnnotationsValidator
    {
        public void TryValidate(object @object)
        {
            IList<ValidationResult> validationResults = new List<ValidationResult>(); 
            var context = new ValidationContext(@object, serviceProvider: null, items: null);
            bool isValid = Validator.TryValidateObject(@object, context, validationResults, validateAllProperties: true);

            if (!isValid)
            {
                StringBuilder errorMessageBuilder = new StringBuilder();

                foreach (ValidationResult validationResult in validationResults)
                    errorMessageBuilder.Append(validationResult.ErrorMessage);

                throw new ValidationException(errorMessageBuilder.ToString());
            }
        }
    }
}
