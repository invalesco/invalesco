﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Invalesco.Application.Service.Exercise;
using Moq;
using Invalesco.Application.Dto.Exercise;
using Invalesco.Core.Model.Common;
using Invalesco.Application.Dto.Common;
using Invalesco.Application.Repository.Exercise;
using Invalesco.Core.Model.Exercise;
using System.Collections.Generic;
using System.Data.Entity;
using Invalesco.Application.Factory;

namespace Invalesco.Tests.Integration.Service
{
    [TestClass]
    public class ExerciseServiceTest : InvalescoIntegrationTest
    {
        #region Setup
        [TestInitialize]
        public void Initialize()
        {
            base.CommonInitialization();
        }
        #endregion

        #region Tests
        [TestMethod]
        public void ExerciseService_Test_Create()
        {
            Mock<DbSet<Exercise>> mockExerciseSet = new Mock<DbSet<Exercise>>();
            MockContext.Setup(x => x.Exercises).Returns(mockExerciseSet.Object);
            IExerciseService exerciseService = InvalescoFactory.GetService<IExerciseService>();

            int expectedExerciseCount = exerciseService.List().Count + 1;

            ExerciseDto newExercise = GetExerciseDto();

            exerciseService.Create(newExercise);

            int actualExerciseCount = exerciseService.List().Count;

            Assert.AreEqual(expectedExerciseCount, actualExerciseCount);
        }
        #endregion

        #region Data
        public ExerciseDto GetExerciseDto()
        {
            ExerciseDto exerciseDto = new ExerciseDto()
            {
                Id = 0,
                Ticks = 0,
                FirstName = "Skwaaaats"
            };

            return exerciseDto;
        }
        #endregion

    }
}
