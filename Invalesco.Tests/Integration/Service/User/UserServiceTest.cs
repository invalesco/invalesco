﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Invalesco.Core.Model.User;
using System.Data.Entity;
using Moq;
using Invalesco.Application.Service.User;
using Invalesco.Application.Factory;
using Invalesco.Application.Dto.User;
using System.ComponentModel.DataAnnotations;

namespace Invalesco.Tests.Integration.Service
{
    [TestClass]
    public class UserServiceTest : InvalescoIntegrationTest
    {
        #region Setup
        [TestInitialize]
        public void Initialize()
        {
            base.CommonInitialization();
        }
        #endregion

        #region Tests
        [TestMethod]
        public void UserService_Register_Pass()
        {
            Mock<DbSet<User>> mockUserSet = new Mock<DbSet<User>>();
            MockContext.Setup(x => x.Users).Returns(mockUserSet.Object);
            IUserService userService = InvalescoFactory.GetService<IUserService>();

            int expectedUserCount = userService.List().Count + 1;

            UserDto newUser = GetUserDto();
            UserDto persistedUser = userService.Register(newUser);
            int actualUserCount = userService.List().Count;

            Assert.IsNotNull(persistedUser.EmailAddress);
            Assert.IsNotNull(persistedUser.Password);
            Assert.IsNotNull(persistedUser.Username);
            Assert.AreEqual(expectedUserCount, actualUserCount);
        }

        [TestMethod]
        public void UserService_Register_Pass_WithoutUsername()
        {
            Mock<DbSet<User>> mockUserSet = new Mock<DbSet<User>>();
            MockContext.Setup(x => x.Users).Returns(mockUserSet.Object);
            IUserService userService = InvalescoFactory.GetService<IUserService>();

            int expectedUserCount = userService.List().Count + 1;

            UserDto newUser = GetUserDto();
            newUser.Username = null;

            userService.Create(newUser);

            int actualUserCount = userService.List().Count;

            Assert.AreEqual(expectedUserCount, actualUserCount);
        }

        [TestMethod]
        [ExpectedException(typeof(ValidationException))]
        public void UserService_Register_Fail_NoEmail()
        {
            Mock<DbSet<User>> mockUserSet = new Mock<DbSet<User>>();
            MockContext.Setup(x => x.Users).Returns(mockUserSet.Object);
            IUserService userService = InvalescoFactory.GetService<IUserService>();

            int expectedUserCount = userService.List().Count + 1;

            UserDto newUser = GetUserDto();
            newUser.EmailAddress = null;

            userService.Create(newUser);

            int actualUserCount = userService.List().Count;

            Assert.AreEqual(expectedUserCount, actualUserCount);
        }

        [TestMethod]
        [ExpectedException(typeof(ValidationException))]
        public void UserService_Register_Fail_EmailInvalid()
        {
            Mock<DbSet<User>> mockUserSet = new Mock<DbSet<User>>();
            MockContext.Setup(x => x.Users).Returns(mockUserSet.Object);
            IUserService userService = InvalescoFactory.GetService<IUserService>();

            int expectedUserCount = userService.List().Count + 1;

            UserDto newUser = GetUserDto();
            newUser.EmailAddress = "abc123";

            userService.Create(newUser);

            int actualUserCount = userService.List().Count;

            Assert.AreEqual(expectedUserCount, actualUserCount);
        }

        [TestMethod]
        [ExpectedException(typeof(ValidationException))]
        public void UserService_Register_Fail_NoPassword()
        {
            Mock<DbSet<User>> mockUserSet = new Mock<DbSet<User>>();
            MockContext.Setup(x => x.Users).Returns(mockUserSet.Object);
            IUserService userService = InvalescoFactory.GetService<IUserService>();

            int expectedUserCount = userService.List().Count + 1;

            UserDto newUser = GetUserDto();
            newUser.Password = null;

            userService.Create(newUser);

            int actualUserCount = userService.List().Count;

            Assert.AreEqual(expectedUserCount, actualUserCount);
        }

        [TestMethod]
        [ExpectedException(typeof(ValidationException))]
        public void UserService_Register_Fail_PasswordMismatch()
        {
            Mock<DbSet<User>> mockUserSet = new Mock<DbSet<User>>();
            MockContext.Setup(x => x.Users).Returns(mockUserSet.Object);
            IUserService userService = InvalescoFactory.GetService<IUserService>();

            int expectedUserCount = userService.List().Count + 1;

            UserDto newUser = GetUserDto();
            newUser.Password = "abc";
            newUser.ConfirmPassword = "123";

            userService.Create(newUser);

            int actualUserCount = userService.List().Count;

            Assert.AreEqual(expectedUserCount, actualUserCount);
        }

        [TestMethod]
        public void UserService_Register_Pass_PasswordIsEncrypted()
        {
            Mock<DbSet<User>> mockUserSet = new Mock<DbSet<User>>();
            MockContext.Setup(x => x.Users).Returns(mockUserSet.Object);
            IUserService userService = InvalescoFactory.GetService<IUserService>();

            UserDto newUser = GetUserDto();
            string unencryptedPassword = newUser.Password;

            UserDto persistedUser = userService.Register(newUser);

            string encryptedPassword = persistedUser.Password;

            Assert.IsNotNull(encryptedPassword);
            Assert.AreNotEqual(unencryptedPassword, encryptedPassword);
        }
        #endregion

        #region Data
        public UserDto GetUserDto()
        {
            return new UserDto()
            {
                Id = 0,
                Ticks = 0,
                FirstName = "Sally",
                SecondName = "Zou",
                Username = "szou",
                EmailAddress = "szszsz@gmail.com",
                Password = "catscats",
                ConfirmPassword = "catscats"
            };
        }
        #endregion
    }
}
