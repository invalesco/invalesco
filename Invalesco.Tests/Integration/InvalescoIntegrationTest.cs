﻿using Invalesco.Application.Context;
using Invalesco.Application.Factory;
using Invalesco.Application.Repository.Exercise;
using Invalesco.Application.Service.Exercise;
using Moq;
using SimpleInjector;
using System;
using System.IO;

namespace Invalesco.Tests.Integration
{
    /// <summary>
    /// Base class to perform common setup routines for integration tests
    /// </summary>
    public class InvalescoIntegrationTest
    {
        public Mock<InvalescoDbContext> MockContext { get; set; }

        public void CommonInitialization()
        {
            AppDomain.CurrentDomain.SetData("DataDirectory", Directory.GetCurrentDirectory());

            InvalescoFactory.Initialize();
            MockContext = new Mock<InvalescoDbContext>();
        }
    }
}
